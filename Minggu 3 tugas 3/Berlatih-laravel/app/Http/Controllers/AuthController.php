<?php

namespace App\Http\Controllers;

class AuthController extends Controller {
  public function index() {
    return view('welcome');
  }
  public function register() {
    return view('register');
  }
  public function registerAction() {
    return redirect()->route('dashboard');
  }
}

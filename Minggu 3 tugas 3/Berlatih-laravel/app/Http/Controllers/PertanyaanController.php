<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index(){
        $questions = DB::table('questions')->get();
        return view('pertanyaan.index', compact('questions'));
    }

    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        $request -> validate([
            'title' => 'required|unique:questions',
            'content' => 'required'
        ]);
        $query = DB::table('questions')
                    ->insert([
                        "title" => $request["title"],
                        "content" => $request["content"]
                    ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan');
    }

    public function show($id){
        $question = DB::table('questions')
                       ->where('id', $id)
                       ->first();
        return view('pertanyaan.show', compact('question'));
    }

    public function edit($id){
        $question = DB::table('questions')
                       ->where('id', $id)
                       ->first();
        return view('pertanyaan.edit', compact('question'));
    }

    public function update($id, Request $request){
        $request -> validate([
            'title' => 'required|unique:questions',
            'content' => 'required'
        ]);
        $query = DB::table('questions')
                    ->where('id', $id)
                    ->update([
                        'title' => $request['title'], 
                        'content' => $request['content']
                    ]);
        return redirect('pertanyaan')->with('success', 'Pertanyaan berhasil diedit');
    }

    public function destroy($id){
        $query = DB::table('questions')
                    ->where('id', $id)
                    ->delete();
        return redirect('pertanyaan')->with('success', 'Pertanyaan berhasil dihapus');
    }
}
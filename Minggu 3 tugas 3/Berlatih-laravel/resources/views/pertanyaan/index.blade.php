@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Daftar Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <a class="btn btn-primary mb-3" href="/pertanyaan/create">Buat pertanyaan baru</a>
            <table class="table table-bordered">
                <thead>                  
                    <tr>
                      <th style="width: 10px">Id</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th style="width: 40px">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($questions as $key => $question)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $question -> title }}</td>
                            <td>{{ $question -> content }}</td>
                            <td style="display:flex">
                                <a href="/pertanyaan/{{ $question->id }}" class="btn btn-info btn-sm">Lihat</a>
                                <a href="/pertanyaan/{{ $question->id }}/edit" class="btn btn-default btn-sm">Edit</a>
                                <form action="/pertanyaan/{{ $question->id }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center">Tidak ada pertanyaan</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection
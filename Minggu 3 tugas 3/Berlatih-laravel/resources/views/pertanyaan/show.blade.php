@extends('layouts.master')

@section('content')
    <div class="card card-body">
        <h4>{{ $question->title }}</h4>
        <p>{{ $question->content }}</p>
    </div>
    
@endsection